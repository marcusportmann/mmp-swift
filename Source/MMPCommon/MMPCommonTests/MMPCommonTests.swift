/*
 * Copyright 2016 Marcus Portmann
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import XCTest
@testable import MMPCommon

class MMPCommonTests: XCTestCase {
  
  /// This method is called before the invocation of each test method in the class.
  override func setUp() {
    super.setUp()
    
    // Put setup code here.
    
  }
  
  /// This method is called after the invocation of each test method in the class.
  override func tearDown() {
    
    // Put teardown code here.
    
    super.tearDown()
  }
  
  /// Test the WBXML functionality.
  func testWBXML() {

    let parentElement = Element(name: "Parent")
    
    parentElement.setAttribute("attributeName1", value: "attributeValue1")
    
    parentElement.setAttribute("attributename2", value: "attributeValue2")
    
    parentElement.setAttribute("attributename2", value: "updatedAttributeValue2")
    
    XCTAssertEqual(true, parentElement.hasAttributes(),
      "Failed to check whether the element has attributes")
    
    XCTAssertEqual(2, parentElement.attributes.count,
      "Failed to check whether the element has 2 attributes")
    
    guard let opaqueChildContent = "Test Data".dataUsingEncoding(NSUTF8StringEncoding) else {
      XCTFail("Faild to retrieve the UTF-8 test data from the String")
      return
    }

    let opaqueChild1Element = Element(name: "OpaqueChild1", content: opaqueChildContent)
    
    parentElement.addContent(opaqueChild1Element)
    
    let opaqueChild2Element = Element(name: "OpaqueChild2")
    
    opaqueChild2Element.setOpaque(opaqueChildContent)
    
    parentElement.addContent(opaqueChild2Element)
    
    let textChild1Element = Element(name: "TextChild1", content: "Text Child 1 Content")
    
    parentElement.addContent(textChild1Element)
    
    let textChild2Element = Element(name: "TextChild2")
    
    textChild2Element.setText("Text Child 2 Content")
    
    parentElement.addContent(textChild2Element)
    
    let document = Document(rootElement: parentElement)
    
    print(document)
    
    
  
    
    
    

    

    
    

    

    
    
    
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
  }
  
  func testPerformanceExample() {
    // This is an example of a performance test case.
    self.measureBlock {
      // Put the code you want to measure the time of here.
    }
  }
  
}
