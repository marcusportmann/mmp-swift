//
// Copyright 2016 Marcus Portmann
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// The CDATA class stores the data for a CDATA (Character Data) content type instance in a WBXML 
/// document.
///
/// This content type is used to store a text string.
public final class CDATA : Content {
  
  /// The text content.
  public var content: String;
  
  /// The textual representation of the CDATA (Character Data) content type instance.
  public var description : String {
    return "<![CDATA[\(content)]]>"
  }
  
  /// Initialise a new CDATA instance.
  /// - Parameter content: The text content.
  public init (content: String) {
    self.content = content;
  }
}