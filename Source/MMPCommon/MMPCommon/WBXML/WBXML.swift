//
// Copyright 2016 Marcus Portmann
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// The WBXML class contains the WBXML constants.
public class WBXML
{
  /// The ID for the ISO 8895-1 character set.
  public static let charsetIso88591 = 0x04;
  
  /// The ID for the UTF-16 character set.
  public static let charsetUtf16 = 0x03F7;
  
  /// The ID for the UTF-8 character set.
  public static let charsetUtf8 = 106;
  
  /// The value signifying an end token (TOKEN_END).
  public static let tokenEnd = 0x01;
  
  /// The value signifying an entity token (TOKEN_ENTITY).
  public static let tokenEntity = 0x02;
  
  /// The value signifying a single-byte document-type-specific extension token (TOKEN_EXT_0).
  public static let tokenSingleByteDocumentTypeSpecificExtension0 = 0xC0;
  
  /// The value signifying a single-byte document-type-specific extension token (TOKEN_EXT_1).
  public static let tokenSingleByteDocumentTypeSpecificExtension1 = 0xC1;
  
  /// The value signifying a single-byte document-type-specific extension token (TOKEN_EXT_2).
  public static let tokenSingleByteDocumentTypeSpecificExtension2 = 0xC2;
  
  /// The value signifying an inline string document-type-specific extension token (TOKEN_EXT_I_0).
  public static let tokenInlineStringDocumentTypeSpecificExtension0 = 0x40;
  
  /// The value signifying an inline string document-type-specific extension token (TOKEN_EXT_I_1).
  public static let tokenInlineStringDocumentTypeSpecificExtension1 = 0x41;
  
  /// The value signifying an inline string document-type-specific extension token (TOKEN_EXT_I_2).
  public static let tokenInlineStringDocumentTypeSpecificExtension2 = 0x42;
  
  /// The value signifying an inline integer document-type-specific extension token (TOKEN_EXT_T_0).
  public static let tokenInlineIntegerDocumentTypeSpecificExtension0 = 0x80;
  
  /// The value signifying an inline integer document-type-specific extension token (TOKEN_EXT_T_1).
  public static let tokenInlineIntegerDocumentTypeSpecificExtension1 = 0x81;
  
  /// The value signifying an inline integer document-type-specific extension token (TOKEN_EXT_T_2).
  public static let tokenInlineIntegerDocumentTypeSpecificExtension2 = 0x82;
  
  /// The value signifying an unknown attribute name, or unknown tag possessing no attributes or
  /// content.
  public static let tokenLiteral = 0x04;
  
  /// The value signifying an unknown tag possessing attributes but no content (TOKEN_LITERAL_A).
  public static let tokenLiteralAttributesOnly = 0x84;
  
  /// The value signifying an unknown tag possessing both attributes and content (TOKEN_LITERAL_AC).
  public static let tokenLiteralAttributesAndContent = 0xC4;
  
  /// The value signifying an unknown tag possessing content but no attributes (TOKEN_LITERAL_C).
  public static let tokenLiteralContentOnly = 0x44;
  
  /// The value signifying an opaque content token (TOKEN_OPAQUE).
  public static let tokenOpaque = 0xC3;
  
  /// The value signifying a processing instruction (TOKEN_PI).
  public static let tokenProcessingInstruction = 0x43;
  
  /// The value signifying an inline string (TOKEN_STR_I).
  public static let tokenInlineString = 0x03;
  
  /// The value signifying a string table reference (TOKEN_STR_T).
  public static let tokenStringTableReference = 0x83;
  
  /// The value signifying a switch page token (TOKEN_SWITCH_PAGE).
  public static let tokenSwitchPage = 0x00;
  
  /// The supported WBXML version.
  public static let wbxmlVersion = 0x01;
}





