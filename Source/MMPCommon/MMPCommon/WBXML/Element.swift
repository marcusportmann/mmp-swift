//
// Copyright 2016 Marcus Portmann
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// The Element class stores the data for a WBXML element content type in a WBXML document.
///
/// This content type represents a node in the WBXML document.
public final class Element : Content {
  
  /// The internal attributes for the element.
  private var _attributes: [String : String] = [String : String]();
  
  /// The attributes for the element.
  public var attributes: [String : String] {
    return _attributes
  }
  
  /// The child elements for the element.
  public var childElements: [Element] {
    return _content.filter{$0 is Element} as! [Element]
  }
  
  /// The internal content for the element.
  private var _content: [Content] = [];

  /// The content for the element.
  public var content: [Content] {
    return _content;
  }
  
  /// The textual representation of the opaque (binary data) content type instance.
  public var description : String {
    
    var buffer = "<"
    
    buffer += name
    
    for attributeName in attributes.keys {
      buffer += " \(attributeName)=\"\(_attributes[attributeName]!)\""
    }

    buffer += ">"
    
    for content in _content {
      buffer += content.description
    }
    
    buffer += "</\(name)>"
    
    return buffer
  }
  
  /// The name of the element.
  public var name: String;
  
  /// The opaque content for the element.
  public var opaque : NSData {
    
    let buffer = NSMutableData()
    
    for content in _content {
      if let opaqueContent = content as? Opaque
      {
        buffer.appendData(opaqueContent.content)
      }
    }
    
    return buffer
  }
  
  /// The text content for the element.
  public var text : String {
    
    var buffer = ""
    
    for content in _content {
      if let cdataContent = content as? CDATA
      {
        buffer += cdataContent.content
      }
    }
    
    return buffer
  }
  
  /// Initialise a new Element instance.
  /// - Parameter name: The name of the element.
  public init (name: String) {
    self.name = name;
  }
  
  /// Initialise a new Element instance.
  /// - Parameter name: The name of the element.
  /// - Parameter content: The binary content for the element.
  public init (name: String, content: NSData) {
    self.name = name;
    addContent(content)
  }
  
  /// Initialise a new Element instance.
  /// - Parameter name: The name of the element.
  /// - Parameter content: The text content for the element.
  public init (name: String, content: String) {
    self.name = name;
    addContent(content)
  }
  
  /// Add the binary content to the element as an opaque content type instance.
  /// - Parameter content: The (opaque) binary data content.
  public func addContent(content: NSData) {
    _content.append(Opaque(content: content))
  }
  
  /// Add the text content to the element as a CDATA content type instance.
  /// - Parameter content: The text content.
  public func addContent(content: String) {
    _content.append(CDATA(content: content))
  }
  
  /// Add the element as a child element.
  /// - Parameter element: The child element.
  public func addContent(element: Element) {
    _content.append(element)
  }
  
  /// Retrieve the attribute with the specified name.
  /// - Returns: The name-value tuple for the attribute with the specified name or nil if the 
  ///     attribute cannot be found.
  /// - Parameter name: The name of the attribute.
  public func getAttribute(name: String) -> (String, String)? {
    if let value = _attributes[name] {
      return (name, value);
    }
    
    return nil
  }
  
  /// Retrieve the String value for the attribute with the specified name.
  /// - Returns: The String value for the attribute with the specified name or nil if the attribute 
  ///     cannot be found.
  /// - Parameter name: The name of the attribute.
  public func getAttributeValue(name: String) -> String? {
    if let value = _attributes[name] {
      return value;
    }
    
    return nil
  }
  
  /// Retrieve the child element with the specified name.
  /// - Returns: The child element with the specified name or nil if the child element cannot be
  ///     found.
  /// - Parameter name: The name of the child element.
  public func getChildElement(name: String) -> Element? {
    for content in _content {
      if let childElement = content as? Element {
        if childElement.name.caseInsensitiveCompare(name) == .OrderedSame {
          return childElement
        }
      }
    }

    return nil
  }
  
  /// Retrieve the (opaque) binary data content for the child element with the specified name.
  /// - Returns: The (opaque) binary data content for the child element with the specified name or 
  ///     nil if the child element cannot be found.
  /// - Parameter name: The name of the child element.
  public func getChildOpaque(name: String) -> NSData? {
    for childElement in childElements {
      if childElement.name.caseInsensitiveCompare(name) == .OrderedSame {
        return childElement.opaque
      }
    }
    
    return nil
  }
  
  /// Retrieve the text content for the child element with the specified name.
  /// - Returns: The text content for the child element with the specified name or nil if the child 
  //      element cannot be found.
  /// - Parameter name: The name of the child element.
  public func getChildText(name: String) -> String? {
    for childElement in childElements {
      if childElement.name.caseInsensitiveCompare(name) == .OrderedSame {
        return childElement.text
      }
    }
    
    return nil
  }
  
  /// Retrieve the child elements with the specified name.
  /// - Returns: The child elements with the specified name.
  /// - Parameter name: The name of the child elements to retrieve.
  public func getChildElements(name: String) -> [Element] {
    return childElements.filter { $0.name.caseInsensitiveCompare(name) == .OrderedSame }
  }
  
  /// Does the element have an attribute with the specified name?
  /// - Returns: true if the element has an attribute with the specified name or false otherwise
  /// - Parameter name: The name of the attribute.
  public func hasAttribute(name: String) -> Bool {
    return _attributes[name] != nil
  }
  
  /// Does the element have attributes?
  /// - Returns: true if the element has attributes or false otherwise
  public func hasAttributes() -> Bool {
    return _attributes.count > 0
  }
  
  /// Does the element have a child element with the specified name?
  /// - Returns: true if the element has a child element with the specified name or false otherwise
  /// - Parameter name: The name of the child element.
  public func hasChildElement(name: String) -> Bool {
    for childElement in childElements {
      if childElement.name == name {
        return true
      }
    }
    
    return false
  }
  
  /// Does the element have children?
  /// - Returns: true if the element has children or false otherwise
  public func hasChildren() -> Bool {
    return childElements.count > 0
  }
  
  /// Does the element have content?
  /// - Returns: true if the element has content of false otherwise
  public func hasContent() -> Bool {
    return _content.count > 0
  }
  
  /// Remove the attribute with the specified name.
  /// - Parameter name: The name of the attribute.
  public func removeAttribute(name: String) {
    _attributes[name] = nil
  }
  
  /// Set the value of the attribute with the specified name.
  /// -Parameter name: The name of the attribute.
  /// -Parameter value: The value of the attribute.
  public func setAttribute(name: String, value: String) {
    _attributes[name] = value
  }
  
  /// Set the (opaque) binary data content for the element.
  /// -Parameter content: The (opaque) binary data content.
  public func setOpaque(content: NSData) {
    // Remove the existing opaque content
    _content = _content.filter { !($0 is Opaque) }
    
    _content.append(Opaque(content: content))
  }
  
  /// Set the text content for the element.
  /// -Parameter content: The text content.
  public func setText(content: String) {
    // Remove the existing text content
    _content = _content.filter { !($0 is CDATA) }
    
    _content.append(CDATA(content: content))
  }
}