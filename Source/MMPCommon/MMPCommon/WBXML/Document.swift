//
// Copyright 2016 Marcus Portmann
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation

/// The Document class represents a WBXML document.
public final class Document : CustomStringConvertible {

  /// The Public ID for an Unknown WBXML document type.
  public static let PublicIdUnknown = 0x01;
  
  /// The textual representation of the WBXML document.
  public var description : String {
    return rootElement.description
  }
  
  /// The root element for the document
  public var rootElement : Element
  
  /// The Public ID for the WBXML document.
  var publicId = PublicIdUnknown
  
  /// Initialise a new Document instance.
  /// - Parameter rootElement: The root element for the document.
  public init (rootElement: Element) {
    self.rootElement = rootElement
  }
  
  /// Initialise a new Document instance.
  /// - Parameter rootElement: The root element for the document.
  /// - Parameter publicId: The Public ID for the document
  public init (rootElement: Element, publicId: Int) {
    self.rootElement = rootElement
    self.publicId = publicId
  }
  
  
  


}